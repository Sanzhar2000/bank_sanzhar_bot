<?php

use Illuminate\Support\Facades\Route;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/' . config('telegram.bot_token') . '/webhook', [MainController::class, 'index']);

Route::get('/test', [MainController::class, 'test']);

Route::get('/set/webhook', function() {
    $response = Telegram::setWebhook([
        'url' => 'https://warm-taiga-51320.herokuapp.com/' . config('telegram.bot_token') . '/webhook'
    ]);

    return $response;
});

Route::get('/remove/webhook', function (){
    $response = Telegram::removeWebhook([
        'url' => 'https://warm-taiga-51320.herokuapp.com/' . config('telegram.bot_token') . '/webhook'
    ]);

    return $response;
});
