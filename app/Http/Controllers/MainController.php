<?php

namespace App\Http\Controllers;

use App\FirstLevelMenu;
use App\LastState;
use App\SecondLevelMenu;
use Illuminate\Http\Request;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;

class MainController extends Controller
{
    protected $telegram;

    public function __construct()
    {
        $this->telegram = new Api(config('telegram.bot_token'));
    }

    public function get_keyboard()
    {
        $array = FirstLevelMenu::pluck('name', 'slug')->toArray();
        $keyboard = [];
        $row = [];
        $count = 0;

        foreach ($array as $key => $value){
            if ($count == 2){
                array_push($keyboard, $row);
                $row = [];
                $count = 0;
            }
            if ($key == array_key_last($array)) {
                array_push($row, $value);
                array_push($keyboard, $row);
                $row = [];
                $count = 0;
                // last element
            } else {
                array_push($row, $value);
                $count++;
                // not last element
            }
        }

        return $this->telegram->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
    }

    public function get_keyboard_2($text)
    {
        $array = FirstLevelMenu::where('name', '=', $text)->first();
        $array = $array->second_level_menus()->pluck('name', 'slug')->toArray();

        $keyboard = [];
        $row = [];

        foreach ($array as $key => $value){
            array_push($row ,$value);
            array_push($keyboard, $row);
            $row = [];
        }

        $keyboard[] = ['Меню'];

        return $this->telegram->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
    }

    public function get_keyboard_3()
    {
        $keyboard = [
            ['Назад'],
            ['Меню']
        ];

        return $this->telegram->replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
    }

    public function test()
    {
        dd(FirstLevelMenu::where('slug', 'about')->first()->text);
    }

    public function index()
    {
        $updates = $this->telegram->getWebhookUpdates();

        $message = $updates->getMessage();
        $user = $message->getFrom();
        $text = $message->getText();

        $first_level_menus = FirstLevelMenu::pluck('name', 'slug')->toArray();
        $second_level_menus = SecondLevelMenu::pluck('name', 'slug')->toArray();

        $second_level_menus_with_third_level = FirstLevelMenu::where('name', 'Депозиты')->orWhere('name', 'Денежные переводы')
            ->with('second_level_menus')->get();
        $array1 = $second_level_menus_with_third_level[0]->second_level_menus->pluck('name')->toArray();
        $array2 = $second_level_menus_with_third_level[1]->second_level_menus->pluck('name')->toArray();

        $second_level_menus_with_third_level = array_merge($array1, $array2);


        if (in_array($text, $first_level_menus)){

            if ($text == 'Депозиты' || $text == 'Денежные переводы'){
                if ($last_state = LastState::where('chat_id', '=', $user->getId())->first()){
                    $last_state->chat_id =  $user->getId();
                    $last_state->last_state = $text;
                    $last_state->save();
                }else{
                    $last_state = new LastState();
                    $last_state->chat_id =  $user->getId();
                    $last_state->last_state = $text;
                    $last_state->save();
                }
            }

            if ($text == 'О банке'){
                $about = FirstLevelMenu::where('slug', 'about')->first();

                $reply_markup = $this->get_keyboard();

                $this->telegram->sendMessage([
                    'chat_id' => $user->getId(),
                    'text' => $about->text,
                    'reply_markup' => $reply_markup,
                ]);
            }else{
                $reply_markup = $this->get_keyboard_2($text);

                $this->telegram->sendMessage([
                    'chat_id' => $user->getId(),
                    'text' => 'Выберите из '. $text . ' меню: ',
                    'reply_markup' => $reply_markup,
                ]);
            }
        }elseif (in_array($text, $second_level_menus)){

            if (in_array($text, $second_level_menus_with_third_level)){

                $reply_markup = $this->get_keyboard_3();

                $this->telegram->sendMessage([
                    'chat_id' => $user->getId(),
                    'text' => 'Выберите из ' . $text .' меню: ',
                    'reply_markup' => $reply_markup,
                ]);
            }else{
                $reply_markup = $this->get_keyboard();

                $this->telegram->sendMessage([
                    'chat_id' => $user->getId(),
                    'text' => 'Выберите из главного меню: ',
                    'reply_markup' => $reply_markup,
                ]);
            }
        }elseif ($text == 'Назад'){

            $last_state = LastState::where('chat_id', '=', $user->getId())->first()->last_state;

            $reply_markup = $this->get_keyboard_2($last_state);

            $this->telegram->sendMessage([
                'chat_id' => $user->getId(),
                'text' => 'Ваша предыдущая страница: ',
                'reply_markup' => $reply_markup,
            ]);
        }else{
            $reply_markup = $this->get_keyboard();

            $this->telegram->sendMessage([
                'chat_id' => $user->getId(),
                'text' => 'Выберите из главного меню: ',
                'reply_markup' => $reply_markup,
            ]);
        }
    }
}
