<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecondLevelMenu extends Model
{
    public function first_level_menu(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(FirstLevelMenu::class);
    }
}
