<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirstLevelMenu extends Model
{
    public function second_level_menus(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(SecondLevelMenu::class, 'first_level_menu_id');
    }
}
