<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSecondLevelMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('second_level_menus', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('first_level_menu_id')->unsigned();
            $table->string('slug');
            $table->string('name');
            $table->timestamps();

            $table->foreign('first_level_menu_id')->references('id')->on('first_level_menus')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('second_level_menus');
    }
}
