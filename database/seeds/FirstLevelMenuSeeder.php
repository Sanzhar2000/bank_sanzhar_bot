<?php

use Illuminate\Database\Seeder;
use App\FirstLevelMenu;

class FirstLevelMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $option1 = new FirstLevelMenu();
        $option1->slug = 'deposits';
        $option1->name = 'Депозиты';
        $option1->save();

        $option2 = new FirstLevelMenu();
        $option2->slug = 'transfers';
        $option2->name = 'Денежные переводы';
        $option2->save();

        $option3 = new FirstLevelMenu();
        $option3->slug = 'departments';
        $option3->name = 'Отделения';
        $option3->save();

        $option4 = new FirstLevelMenu();
        $option4->slug = 'atm';
        $option4->name = 'Банкоматы';
        $option4->save();

        $option5 = new FirstLevelMenu();
        $option5->slug = 'about';
        $option5->name = 'О банке';
        $option5->text = 'Банк Sanzhar специализируется на предоставлении финансовых услуг населению и малым предприятиям.' . "\n" . nl2br() .
                        'Через нашу филиальную сеть мы предлагаем простые и понятные продукты по разумной цене.' . "\n" . nl2br() .
                        'Наша бизнес-этика простая и ясная: быстрый, доступный и инновационный банк.' . "\n" . nl2br() .
                        'Цель банка - стать одним из лидеров в своем сегменте, а именно в предоставлении финансовых услуг широкому кругу населения и малому бизнесу.' . "\n" . nl2br() .
                        'В среднесрочной перспективе планируется обеспечить присутствие Банка во всех крупных городах страны.' . "\n";
        $option5->save();
    }
}
