<?php

use Illuminate\Database\Seeder;

use App\FirstLevelMenu;
use App\SecondLevelMenu;

class SecondLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //1st Level

        $first_level_menu_1 = FirstLevelMenu::where('slug', '=', 'deposits')->first();
        $first_level_menu_2 = FirstLevelMenu::where('slug', '=', 'transfers')->first();
        $first_level_menu_3 = FirstLevelMenu::where('slug', '=', 'departments')->first();
        $first_level_menu_4 = FirstLevelMenu::where('slug', '=', 'atm')->first();
        //$first_level_menu_5 = FirstLevelMenu::where('slug', '=', 'about')->first(;

        //Deposits

        $option1 = new SecondLevelMenu();
        $option1->slug = 'novamoney';
        $option1->name = '#KASSANOVAMONEY Возможность частичного изъятия';
        $first_level_menu_1->second_level_menus()->save($option1);

        $option2 = new SecondLevelMenu();
        $option2->slug = 'novamxai';
        $option2->name = '#KASSANOVAMAXI Без возможности частичного изъятия';
        $first_level_menu_1->second_level_menus()->save($option2);

        $option3 = new SecondLevelMenu();
        $option3->slug = 'contingent-deposits';
        $option3->name = 'Условные вклады для юридических лиц';
        $first_level_menu_1->second_level_menus()->save($option3);

        $option4 = new SecondLevelMenu();
        $option4->slug = 'deposit-overnight';
        $option4->name = 'Депозит Овернайт';
        $first_level_menu_1->second_level_menus()->save($option4);

        $option5 = new SecondLevelMenu();
        $option5->slug = 'deposit-nova-capital';
        $option5->name = 'Депозит #KASSANOVЫЙКАПИТАЛ';
        $first_level_menu_1->second_level_menus()->save($option5);

        $option6 = new SecondLevelMenu();
        $option6->slug = 'deposit-nova-strategy';
        $option6->name = 'Депозит #KASSANOVАЯСТРАТЕГИЯ';
        $first_level_menu_1->second_level_menus()->save($option6);

        $option7 = new SecondLevelMenu();
        $option7->slug = 'deposit-nova-moneybox';
        $option7->name = 'Депозит #KASSANOVAЯКОПИЛКА';
        $first_level_menu_1->second_level_menus()->save($option7);


        //Money Transfers

        $option1 = new SecondLevelMenu();
        $option1->slug = 'gold-corona';
        $option1->name = 'Золотая корона';
        $first_level_menu_2->second_level_menus()->save($option1);

        $option2 = new SecondLevelMenu();
        $option2->slug = 'western-union';
        $option2->name = 'Western Union';
        $first_level_menu_2->second_level_menus()->save($option2);

        $option3 = new SecondLevelMenu();
        $option3->slug = 'swift';
        $option3->name = 'Swift';
        $first_level_menu_2->second_level_menus()->save($option3);

        $option4 = new SecondLevelMenu();
        $option4->slug = 'unistream';
        $option4->name = 'Юнистрим';
        $first_level_menu_2->second_level_menus()->save($option4);


        //Departments

        $option1 = new SecondLevelMenu();
        $option1->slug = 'dep-shymkent';
        $option1->name = 'Шымкент';
        $first_level_menu_3->second_level_menus()->save($option1);

        $option2 = new SecondLevelMenu();
        $option2->slug = 'dep-kokshetau';
        $option2->name = 'Кокшетау';
        $first_level_menu_3->second_level_menus()->save($option2);

        $option3 = new SecondLevelMenu();
        $option3->slug = 'dep-pavlodar';
        $option3->name = 'Павлодар';
        $first_level_menu_3->second_level_menus()->save($option3);

        $option4 = new SecondLevelMenu();
        $option4->slug = 'dep-nur-sultan';
        $option4->name = 'Нур-Султан';
        $first_level_menu_3->second_level_menus()->save($option4);

        $option5 = new SecondLevelMenu();
        $option5->slug = 'dep-karaganda';
        $option5->name = 'Караганда';
        $first_level_menu_3->second_level_menus()->save($option5);

        $option6 = new SecondLevelMenu();
        $option6->slug = 'dep-aktobe';
        $option6->name = 'Актобе';
        $first_level_menu_3->second_level_menus()->save($option6);

        $option7 = new SecondLevelMenu();
        $option7->slug = 'dep-almaty';
        $option7->name = 'Алматы';
        $first_level_menu_3->second_level_menus()->save($option7);

        $option8 = new SecondLevelMenu();
        $option8->slug = 'dep-ust-kamenogorsk';
        $option8->name = 'Усть - Каменогорск';
        $first_level_menu_3->second_level_menus()->save($option8);


        //ATMs

        $option1 = new SecondLevelMenu();
        $option1->slug = 'atm-shymkent';
        $option1->name = 'Шымкент';
        $first_level_menu_4->second_level_menus()->save($option1);

        $option2 = new SecondLevelMenu();
        $option2->slug = 'atm-kokshetau';
        $option2->name = 'Кокшетау';
        $first_level_menu_4->second_level_menus()->save($option2);

        $option3 = new SecondLevelMenu();
        $option3->slug = 'atm-pavlodar';
        $option3->name = 'Павлодар';
        $first_level_menu_4->second_level_menus()->save($option3);

        $option4 = new SecondLevelMenu();
        $option4->slug = 'atm-nur-sultan';
        $option4->name = 'Нур-Султан';
        $first_level_menu_4->second_level_menus()->save($option4);

        $option5 = new SecondLevelMenu();
        $option5->slug = 'atm-karaganda';
        $option5->name = 'Караганда';
        $first_level_menu_4->second_level_menus()->save($option5);

        $option6 = new SecondLevelMenu();
        $option6->slug = 'atm-aktobe';
        $option6->name = 'Актобе';
        $first_level_menu_4->second_level_menus()->save($option6);

        $option7 = new SecondLevelMenu();
        $option7->slug = 'atm-almaty';
        $option7->name = 'Алматы';
        $first_level_menu_4->second_level_menus()->save($option7);

        $option8 = new SecondLevelMenu();
        $option8->slug = 'atm-ust-kamenogorsk';
        $option8->name = 'Усть - Каменогорск';
        $first_level_menu_4->second_level_menus()->save($option8);
    }
}
